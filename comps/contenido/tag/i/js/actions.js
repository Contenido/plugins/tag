var nTagsFormEditing = 0;

$(document).ready(function(){

    $('#js-add-tag').on('click', function( ev ){
	ev.preventDefault();
	OpenContentForm( 'new' );
    });

});

function OpenContentForm ( nId ) {
	if ( nTagsFormEditing > 0 && confirm( 'Несохраненные данные в форме редактирования тега. Сбросить и продолжить?' ) ) {
		nTagsFormEditing = 0;
	}
	if ( nTagsFormEditing == 0 ) {
		$('.content-main').load('/contenido/tag/ajax/tag_edit_form.html?id=' + nId );
	}
}