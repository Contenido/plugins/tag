package tag::Apache;

use strict;
use warnings 'all';

use tag::State;
use Contenido::Globals;


sub child_init {
	# встраиваем keeper плагина в keeper проекта
	$keeper->{tag} = tag::Keeper->new($state->tag);
}

sub request_init {
}

sub child_exit {
}

1;
