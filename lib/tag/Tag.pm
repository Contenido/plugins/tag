package tag::Tag;

use base 'Contenido::Document';
use Contenido::Globals;

sub extra_properties
{
   return (
		{ 'attr' => 'name',					'rusname' => 'Название тега', shortname => 'Тег' },
		{ 'attr' => 'status',
			cases	=> [
				[0, 'Скрытый'],
				[1, 'Активный'],
				[-1, 'Удален'],
				
			],
		},
		{ 'attr' => 'alt_name',		type => 'string',	'rusname' => 'Альтернативная форма названия', shortname => 'Словоформа' },
		{ 'attr' => 'abstr',		type => 'text',		'rusname' => 'Краткая аннотация', rows => 5 },
	)
}

sub get_objects
{
	my $self = shift;
	return	unless $self->id;

	my (%opts) = @_;
	return	unless exists $opts{class};

	my $cache = delete $opts{cache};
	my $key = 'objects_class_'.$opts{class}.'_by_tag_'.$self->id;
	my $objects = $cache && $state->memcached_enable ? $keeper->MEMD->get( $key ) : undef;
	unless ( ref $objects ) {
		$objects = $keeper->get_documents(
				%opts,
				lclass	=> 'tag::Cloud',
				lsource	=> $self->id,
				return_mode	=> 'array_ref',
			);
		if ( $cache && $state->memcached_enable ) {
			map { $_->{keeper} = undef } @$objects;
			$keeper->MEMD->set( $key, $objects );
		}
	}
	if ( $cache && $state->memcached_enable && ref $objects eq 'ARRAY' ) {
		map { $_->{keeper} = $keeper } @$objects;
	}
	return $objects;
}

sub class_name
{
	return 'Тег';
}

sub class_description
{
	return 'Тег';
}

sub class_table
{
	return 'tag::SQL::TagsTable';
}

sub search_fields {
	return ('name');
}

sub pre_store
{
	my $self = shift;

	for ( $self->{name} ) {
		s/^\s+//;
		s/\s+$//;
	}

	if ( !$self->sections && ref $project->s_alias eq 'HASH' && exists $project->s_alias->{tags} ) {
		my $default_section = $project->s_alias->{tags};
		$self->sections( $default_section )			if $default_section;
	}

	return 1;
}

sub post_delete
{
	my $self = shift;

	my $links = $self->keeper->get_links(
			class   => 'tag::Cloud',
			source_id       => $self->id,
			return_mode	=> 'array_ref',
		);
	if ( ref $links eq 'ARRAY' &&  @$links ) {
		foreach my $link ( @$links ) {
			$link->delete;
		}
	}
}

1;
