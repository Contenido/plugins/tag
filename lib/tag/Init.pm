package tag::Init;

use strict;
use warnings 'all';

use Contenido::Globals;
use tag::Apache;
use tag::Keeper;
use tag::Section;
use tag::Tag;
use tag::Cloud;
use tag::SQL::TagsCloudTable;
use tag::SQL::TagsTable;

# загрузка всех необходимых плагину классов
# tag::SQL::SomeTable
# tag::SomeClass
Contenido::Init::load_classes(qw(
		tag::Section
		tag::Tag
		tag::Cloud
		tag::SQL::TagsCloudTable
		tag::SQL::TagsTable
	));

sub init {
	push @{ $state->{'available_documents'} },
		qw (
			tag::Tag
		);
	push @{ $state->{'available_sections'} },
		qw (
			tag::Section
		);
	push @{ $state->{'available_links'} },
		qw (
			tag::Cloud
		);
	0;
}

1;
