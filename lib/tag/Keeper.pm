package tag::Keeper;

use strict;
use warnings 'all';
use base qw(Contenido::Keeper);

use Contenido::Globals;

sub get_tree {
    my $self = shift;
    my (%opts) = @_;
    my $level;
    if ( exists $opts{level} && $opts{level} ) {
	$level = [1..$opts{level}];
	delete $opts{level};
    } else {
	$level = [1,2];
    }
    my $cache = delete $opts{cache};

    my $tree;
    my $cache_key = 'plugin_tag_tree_level_'.join('_', @$level);
    if ( $cache > 0 && $keeper->MEMD ) {
	$tree = $keeper->MEMD->get( $cache_key );
    }
    unless ( defined $tree ) {
	$tree = { hash => {}, root => [] };
	my $tags = $keeper->get_documents(
			class	=> 'tag::Tag',
			level	=> $level,
			%opts,
			order_by	=> 'level, pid, id',
			return_mode	=> 'array_ref',
		);
	foreach my $tag ( @$tags ) {
		$tag->{keeper} = undef;
		$tree->{hash}{$tag->id} = $tag;
		if ( $tag->pid ) {
			push @{$tree->{$tag->pid}}, $tag;
			if ( exists $tree->{hash}{$tag->pid} ) {
				push @{$tree->{hash}{$tag->pid}{children}}, $tag;
			}
		} else {
			push @{$tree->{root}}, $tag;
		}
		if ( $cache && $keeper->MEMD ) {
			$keeper->MEMD->set( $cache_key, $tree, 3600 );
		}
	}
    }
    return $tree;
}

sub store_extras {
    my $self = shift;
    my $object = shift;

    if ( ref $object && $object->id ) {
	my ($prop) = grep { $_->{type} eq 'tagset' } $object->structure;
	if ( ref $prop && !(exists $prop->{virtual} && $prop->{virtual}) ) {
		my $name = $prop->{attr};
		my $class = $object->class;
		my $is_extra = scalar( grep { ref $_ && $_->{attr} eq $name } $class->class_table->required_properties ) ? 0 : 1;

		my $result;
		my @tags;
		@tags = $keeper->get_documents(
				class   => 'tag::Tag',
				lclass  => 'tag::Cloud',
				ldest   => $object->id,
				ldestclass      => $object->class,
			);
		if ( @tags ) {
			$result = [];
			foreach my $tag ( @tags ) {
				push @$result, { id => $tag->id, name => $is_extra ? $tag->name : Encode::decode('utf-8', $tag->name) };
			}
			unless ( $is_extra ) {
				$result = Encode::encode('utf-8', JSON::XS->new->encode( $result ));
			}
		}
		warn Data::Dumper::Dumper $result             if $DEBUG;
		$object->$name( $result );
		$object->store;
	}
    }
}

1;
