package tag::SQL::TagsTable;

use strict;
use base 'SQL::DocumentTable';

sub db_table
{
	return 'tags';
}

sub available_filters {
	my @available_filters = qw(	
					_class_filter
					_status_filter
					_in_id_filter
					_id_filter
					_name_filter
					_class_excludes_filter
					_prev_to_filter
					_next_to_filter
					_s_filter

					_excludes_filter
					_link_filter
					_pid_filter
					_level_filter
					_alias_filter
				);
	return \@available_filters; 
}

# ----------------------------------------------------------------------------
# Свойства храним в массивах, потому что порядок важен!
# Это общие свойства - одинаковые для всех документов.
#
#   attr - обязательный параметр, название атрибута;
#   type - тип аттрибута, требуется для отображдения;
#   rusname - русское название, опять же требуется для отображения;
#   hidden - равен 1, когда 
#   readonly - инициализации при записи только без изменения в дальнейшем
#   db_field - поле в таблице
#   default  - значение по умолчанию (поле всегда имеет это значение)
# ----------------------------------------------------------------------------
sub required_properties
{
	my $self = shift;

	my @parent_properties = grep { $_->{attr} ne 'dtime' && $_->{attr} ne 'sections' } $self->SUPER::required_properties;
	return (
		@parent_properties,
		{
			'attr'		=> 'alias',
			'type'		=> 'string',
			'rusname'	=> 'Алиас',
			'column'	=> 3,
			'db_field'	=> 'alias',
			'db_type'	=> 'text',
		},
		{
			'attr'		=> 'level',
			'type'		=> 'integer',
			'rusname'	=> 'Уровень в иерархии',
			'rem'		=> '1 - верхний уровень. 2..4 - вложенные уровни. 0 - для одноуровневой структуры и деклассированных тегов, не включенных в иерархию.',
			'db_field'	=> 'level',
			'db_type'	=> 'integer',
			'default'	=> 0,
		},
		{
			'attr'		=> 'pid',
			'type'		=> 'pickup',
			'rusname'	=> 'Родительский тег',
			'lookup_opts'	=> {
				class	=> 'tag::Tag',
				search_by	=> 'name',
			},
			'db_field'	=> 'pid',
			'db_type'	=> 'integer',
			'default'	=> 0,
		},
		{
			'attr'		=> 'sections',
			'type'		=> 'sections_list',
			'rusname'	=> 'Секция',
			'hidden'	=> 1,
			'db_field'	=> 'sections',
			'db_type'	=> 'integer',
		},
	);
}

########### FILTERS DESCRIPTION ####################################################################################
sub _get_orders {
	my ($self, %opts) = @_;

	if ($opts{order_by}) {
		return ' order by '.$opts{order_by};
	} else {
		return ' order by name';
	}
	return undef;
}

sub _s_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{s} && defined $opts{s} );
	return &SQL::Common::_generic_int_filter('d.sections', $opts{s});
}

sub _alias_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{alias} );
	return &SQL::Common::_generic_text_filter('d.alias', $opts{alias});
}

sub _level_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{level} );
	return &SQL::Common::_generic_int_filter('d.level', $opts{level});
}

sub _pid_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{pid} );
	return &SQL::Common::_generic_text_filter('d.pid', $opts{pid});
}

sub _link_filter {
	my ($self,%opts)=@_;

	my @wheres=();
	my @binds=();

	# Связь определенного класса
	if (exists($opts{lclass})) {
		my ($where, $values) = SQL::Common::_generic_text_filter('l.class', $opts{lclass});
		push (@wheres, $where);
		push (@binds,   ref($values) ? @$values:$values) if (defined $values);
	}

	my $lclass = $opts{lclass} || 'Contenido::Link';
	my $link_table = $lclass->_get_table->db_table();

	# Ограничение по статусу связи
	if ( exists $opts{lstatus} ) {
		my ($where, $values) = SQL::Common::_generic_int_filter('l.status', $opts{lstatus});
		push (@wheres, $where);
		push (@binds,   ref($values) ? @$values:$values) if (defined $values);
	}

	# Связь с определенным документ(ом/тами) по цели линка
	if ( exists $opts{ldest} ) {
		my ($where, $values) = SQL::Common::_generic_int_filter('l.dest_id', $opts{ldest});
		push (@wheres, $where);
		push (@binds,   ref($values) ? @$values:$values) if (defined $values);
		if ($self->_single_class) {
			return (\@wheres, \@binds, " join $link_table as l on l.source_id=d.id");
		} elsif ( exists $opts{ldestclass} ) {
			my ($where, $values) = SQL::Common::_generic_text_filter('l.dest_class', $opts{ldestclass});
			push (@wheres, $where);
			push (@binds,   ref($values) ? @$values:$values) if (defined $values);

			return (\@wheres, \@binds, " join $link_table as l on l.source_id=d.id and l.source_class=d.class");
		} else {
			return (\@wheres, \@binds, " join $link_table as l on l.source_id=d.id and l.source_class=d.class");
		}
	}

	# Связь с определенным документ(ом/тами) по источнику линка
	if ( exists $opts{lsource} ) {
		my ($where, $values) = SQL::Common::_generic_int_filter('l.source_id', $opts{lsource});
		push (@wheres, $where);
		push (@binds,   ref($values) ? @$values:$values) if (defined $values);
		if ($self->_single_class) {
			return (\@wheres, \@binds, " join $link_table as l on l.dest_id=d.id");
		} elsif ( exists $opts{lsourceclass} ) {
			my ($where, $values) = SQL::Common::_generic_text_filter('l.source_class', $opts{lsourceclass});
			push (@wheres, $where);
			push (@binds,   ref($values) ? @$values:$values) if (defined $values);

			return (\@wheres, \@binds, " join $link_table as l on l.dest_id=d.id and l.dest_class=d.class");
		} else {
			return (\@wheres, \@binds, " join $link_table as l on l.dest_id=d.id and l.dest_class=d.class");
		}
	}

	# Связь с определенным документ(ом/тами) по классам линка
	if ( exists $opts{ldestclass} ) {
		my ($where, $values) = SQL::Common::_generic_text_filter('l.dest_class', $opts{ldestclass});
		push (@wheres, $where);
		push (@binds,   ref($values) ? @$values:$values) if (defined $values);

		return (\@wheres, \@binds, " join $link_table as l on l.source_id=d.id and l.source_class=d.class");
	}


	return (undef);
}


1;
