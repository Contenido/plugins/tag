package tag::Section;

use base 'Contenido::Section';

sub extra_properties
{
	return (
		{ 'attr' => 'default_document_class',			'default' => 'tag::Tag' },
	)
}

sub class_name
{
	return 'Секция тегов';
}

sub class_description
{
	return 'Секция тегов';
}

1;
