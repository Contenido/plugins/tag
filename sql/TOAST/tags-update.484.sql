alter table tags add column pid integer default 0;
alter table tags add column level integer default 1;
create index tags_pid on tags (pid);
