alter table tags add column pid integer default 0;
alter table tags add column level integer;

create index tags_pid on tags (pid) WHERE pid IS NOT NULL;
