create table tags_cloud
(
	id integer not null primary key default nextval('public.documents_id_seq'::text),
	class text not null,
	ctime timestamp not null default now(),
	mtime timestamp not null default now(),
	status smallint not null default 1,
	source_id integer not null,
	source_class text not null default 'tags::Tag',
	dest_id integer not null,
	dest_class text not null,
	data text
);
create index tags_cloud_source on tags_cloud (source_id);
create index tags_cloud_dest on tags_cloud (dest_id);
