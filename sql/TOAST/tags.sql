create table tags
(
	id integer not null primary key default nextval('public.documents_id_seq'::text),
	ctime timestamp not null default now(),
	mtime timestamp not null default now(),
	class text not null,
	status smallint not null default 0,
	sections integer,
	pid integer default 0,
	level integer default 0,
	name text,
	alias text,
	data text
);
create index tags_name on tags (name);
create index tags_alias on tags (alias) WHERE alias IS NOT NULL AND alias != '';
create index tags_pid on tags (pid) WHERE level > 0;
