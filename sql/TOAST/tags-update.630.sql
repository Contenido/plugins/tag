alter table tags alter column level set default 0;
update tags set level = 0 where level is null;

drop index tags_pid;
create index tags_pid on tags (pid) WHERE level > 0;
